<?php

namespace App\Services;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;
use App\Entity\Badge;


class BadgeService
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function setBadge(User $user, string $badgeName)
    {
        if(!$this->entityManager->getRepository(Badge::class)->findOneBy(['user' => $user->getId(), 'name' => $badgeName]) && $this->isValidBadge($badgeName))
        {
            $badge = new Badge();
            $badge->setUser($user);
            $badge->setName($badgeName);
            $this->entityManager->persist($badge);
            $this->entityManager->flush();
        }
        return;
    }

    public function isValidBadge(string $badge) : ?bool
    {
        switch($badge)
        {
            case 'Rookie artist':
                return true;
            case 'Rockstart! 5 albums':
                return true;
            case 'First song':
                return true;
            case '10 songs':
                return true;
            case 'First comment':
                return true;
            case '10 comments!':
                return true;
            case 'Knows a lot about music':
                return true;
            case 'First report':
                return true;
            case 'Donation':
                return true;
            case 'Editor mode':
                return true;
            case 'First like':
                return true;
            case 'First favorite album':
                return true;
            default:
                return false;
        }
    }
}