<?php

namespace App\Services;

use App\Entity\Album;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Dotenv\Dotenv;

class DonationService
{
    private $entityManager;
    private $badgeService;
    private $container;
    private $router;
    private $session;

    /**
     * DonationService constructor.
     * @param EntityManagerInterface $entityManager
     * @param BadgeService $badgeService
     * @param ContainerInterface $container
     * @param RouterInterface $router
     */
    public function __construct(EntityManagerInterface $entityManager, BadgeService $badgeService, ContainerInterface $container, RouterInterface $router)
    {
        $this->entityManager = $entityManager;
        $this->badgeService = $badgeService;
        $this->container = $container;
        $this->router = $router;
        $this->session = $this->container->get('session');
    }
    /**
     * @param $token
     * @param $albumName
     * @param $amount
     * @return bool
     */
    public function payment($token, string $albumName, float $amount)
    {
        $dotenv = new Dotenv();
        $dotenv->load('../'.'/.env');

        $stripe = array(
            "secret_key"      => $_ENV['STRIPE_SECRET_KEY'],
            "publishable_key" => $_ENV['STRIPE_PUBLIC_KEY']
        );
        \Stripe\Stripe::setApiKey("sk_test_4LaQ23OMNso4KLK8nuodJZlK00FAvPf9Rr");
        $customer = \Stripe\Customer::create(array(
            'source' => $token,
        ));
        $charge = \Stripe\Charge::create([
            'amount' => $amount,
            'currency' => 'eur',
            'description' => $albumName,
            'source' => 'tok_visa_debit',
        ]);
        $chargeJson = $charge->jsonSerialize();
        if($chargeJson['amount_refunded'] != 0 || !empty($chargeJson['failure_code']) || $chargeJson['paid'] != 1 || $chargeJson['captured'] != 1) {
            return false;
        }
        return true;
    }

    /**
     * @param User $user
     * @param $amount
     */
    public function changeBalance(User $user, $amount)
    {
        $curr_amount = $user->getBalance();
        $new_balance = $curr_amount + $amount;
        $user->setBalance($new_balance);
        $this->entityManager->flush();
        return;
    }

    /**
     * @param User $user
     * @return array
     */
    public function getUserAlbums(User $user)
    {
        $albums = [];
        foreach($user->getAlbums() as $album) {
            $albums[] = $album;
        }
        return $albums;
    }

    /**
     * @param Album $album
     * @param User $user
     * @return RedirectResponse
     */
    public function increaseRating(Album $album, User $user)
    {
        if($user->getBalance() < 350) {
            $this->session->getFlashBag()->add('store', 'You dont enough money in your balance.');
            return new RedirectResponse($this->router->generate('store_index'));
        }
        if($album->getRating() == 0) {
            $album->setRating(1);
            $this->entityManager->flush();
        }
        else {
            $rating = ($album->getRating()*10)/100;
            $album->setRating($album->getRating() + $rating);
            $this->entityManager->flush();
        }
        $this->changeBalance($user, -350);
        $this->badgeService->setBadge($user, "Store");
        $this->session->getFlashBag()->add('store', 'Great! You bought 10% rating increase for your album!');
        return;
    }

    /**
     * @param Album $album
     * @param User $user
     * @return RedirectResponse
     */

    public function goldenLogo(Album $album, User $user)
    {
        if($user->getBalance() < 150) {
            $this->session->getFlashBag()->add('store', 'You dont enough money in your balance.');
            return new RedirectResponse($this->router->generate('store_index'));
        }
        if($album->getGoldenLogo() == 0) {
            $this->badgeService->setBadge($user, "Store");
            $this->session->getFlashBag()->add('store', 'You bought golden logo!');
            $album->setGoldenlogo(1);
            $this->entityManager->flush();
            $this->changeBalance($user, -150);
            $this->entityManager->flush();
        }
        else
        {
            $this->session->getFlashBag()->add('store', 'This album already have golden logo.');
            return new RedirectResponse($this->router->generate('store_index'));
        }
        return;
    }
}