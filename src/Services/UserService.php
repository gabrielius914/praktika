<?php

namespace App\Services;


use App\Entity\Album;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class UserService
{
    private $security;
    private $entityManager;

    public function __construct(Security $security, EntityManagerInterface $entityManager)
    {
        $this->security = $security;
        $this->entityManager = $entityManager;
    }

    public function isLoggedIn() : ?bool
    {
        if ($this->security->isGranted('IS_AUTHENTICATED_FULLY'))
            return true;
        return false;
    }

    public function isAdmin() : ?bool
    {
        if ($this->security->isGranted('ROLE_ADMIN'))
            return true;

        return false;
    }

    public function isArtist() : ?bool
    {
        if(!$this->security->isGranted('IS_AUTHENTICATED_FULLY'))
            return false;
        if ($this->security->isGranted('ROLE_ARTIST'))
        {
            return true;
        }
        return false;
    }

    public function isOwner(Album $album, User $user) : ?bool
    {
        if($this->security->isGranted('ROLE_ADMIN'))
        {
            return true;
        }
        else if($this->security->isGranted('ROLE_ARTIST'))
        {
            if(!$user) return false;

            if($user->getAlbums()->contains($album))
                return true;
        }
        return false;
    }

    public function isFavourited(Album $album, User $user) : ?bool
    {
        if(!$this->security->isGranted('IS_FULLY_AUTHENTICATED'))
            return false;
        if(!$user) return false;
        if($user->getFavourites()->contains($album))
            return true;

        return false;
    }

    public function isLiked(Album $album, User $user) : ?bool
    {
        if(!$this->security->isGranted('IS_FULLY_AUTHENTICATED'))
            return false;
        if(!$user) return false;
        if($user->getLikes()->contains($album))
            return true;

        return false;
    }

    public function deleteUserData(User $user)
    {

        foreach($user->getDonations() as $donation)
        {
            $this->entityManager->remove($donation);
            $this->entityManager->flush();
        }
        foreach($user->getReports() as $report)
        {
            $this->entityManager->remove($report);
            $this->entityManager->flush();
        }

        foreach($user->getComments() as $comment)
        {
            $this->entityManager->remove($comment);
            $this->entityManager->flush();
        }

        foreach($user->getLikes() as $like)
        {
            $this->entityManager->remove($like);
            $this->entityManager->flush();
        }
        foreach($user->getFavourites() as $favourite)
        {
            $this->entityManager->remove($favourite);
            $this->entityManager->flush();
        }
        foreach($user->getActivities() as $activity)
        {
            $this->entityManager->remove($activity);
            $this->entityManager->flush();
        }

        foreach($user->getBadges() as $badge)
        {
            $this->entityManager->remove($badge);
            $this->entityManager->flush();
        }

        $albums = $user->getAlbums();
        foreach($albums as $album)
        {
            foreach($album->getSongs() as $song)
            {
                $this->entityManager->remove($song);
                $this->entityManager->flush();
            }

            foreach($album->getComments() as $comment)
            {
                $this->entityManager->remove($comment);
                $this->entityManager->flush();
            }

            foreach($album->getLikes() as $like)
            {
                $this->entityManager->remove($like);
                $this->entityManager->flush();
            }

            foreach($album->getFavourites() as $favourite)
            {
                $this->entityManager->remove($favourite);
                $this->entityManager->flush();
            }

            foreach($album->getReports() as $report)
            {
                $this->entityManager->remove($report);
                $this->entityManager->flush();
            }

            foreach($album->getDonations() as $donation)
            {
                $this->entityManager->remove($donation);
                $this->entityManager->flush();
            }

            $this->entityManager->remove($album);
            $this->entityManager->flush();

        }

    }

}