<?php

namespace App\Services;


use App\Entity\Album;
use App\Entity\Comment;
use App\Entity\Donation;
use App\Entity\Favourite;
use App\Entity\Report;
use App\Entity\Song;
use App\Entity\User;
use App\Entity\Like;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\RouterInterface;


class AlbumService
{

    private $entityManager;
    private $activityService;
    private $badgeService;
    private $donationService;
    private $session;
    private $container;
    private $router;

    /**
     * AlbumService constructor.
     * @param EntityManagerInterface $entityManager
     * @param ActivityService $activityService
     * @param BadgeService $badgeService
     * @param DonationService $donationService
     * @param ContainerInterface $container
     * @param RouterInterface $router
     */
    public function __construct(EntityManagerInterface $entityManager, ActivityService $activityService, BadgeService $badgeService, DonationService $donationService, ContainerInterface $container, RouterInterface $router)
    {
        $this->entityManager = $entityManager;
        $this->activityService = $activityService;
        $this->badgeService = $badgeService;
        $this->donationService = $donationService;
        $this->container = $container;
        $this->session = $this->container->get('session');
        $this->router = $router;
    }

    /**
     * @param Album $album
     */
    public function deleteAllAlbumData(Album $album)
    {
        $all_likes = $album->getLikes();
        foreach($all_likes as $like)
        {
            $this->entityManager->remove($like);
            $album->removeLike($like);
        }
        $all_favourites = $album->getFavourites();
        foreach($all_favourites as $fav)
        {
            $this->entityManager->remove($fav);
            $album->removeFavourite($fav);
        }
        $all_songs = $album->getSongs();
        foreach($all_songs as $song)
        {
            $this->entityManager->remove($song);
            $album->removeSong($song);
        }
        $allcomments = $album->getComments();
        foreach($allcomments as $comment)
        {
            $this->entityManager->remove($comment);
            $album->removeComment($comment);
        }
        $allreports = $album->getReports();
        foreach($allreports as $report)
        {
            $this->entityManager->remove($report);
            $album->removeReport($report);
        }
        $this->entityManager->remove($album);
        $this->entityManager->flush();
        return;
    }

    /**
     * @param $album_id
     * @param User $userEntity
     * @return bool
     */
    public function setLikeForUserAndAlbum(int $album_id, User $user) : ?bool
    {
        $album = $this->entityManager->getRepository(Album::class)->findOneBy(['id' => $album_id]);
        if(!$album) return true;

        if($user->isLiked($album))
            return true;
        $like = new Like();
        $like->setUser($user);
        $like->setAlbum($album);
        $this->entityManager->persist($like);
        $this->entityManager->flush();
        $user->addLike($like);
        $this->entityManager->flush();
        $album->addLike($like);
        $this->entityManager->flush();
        return false;
    }

    /**
     * @param $album_id
     * @param User $userEntity
     * @return bool
     */
    public function removeLikeForUserAndAlbum(int $album_id, User $user) : ?bool
    {
        $album = $this->entityManager->getRepository(Album::class)->findOneBy(['id' => $album_id]);
        if(!$album) return true;
        $like = $this->entityManager->getRepository(Like::class)->findOneBy(['user' => $user->getId(), 'album' => $album->getId()]);
        if(!$user->isLiked($album))
            return false;
        $this->entityManager->remove($like);
        $this->entityManager->flush();
        return true;
    }

    /**
     * @param $album_id
     * @param User $userEntity
     * @return bool
     */
    public function addToFavorites(int $album_id, User $user) : ?bool
    {
        $album = $this->entityManager->getRepository(Album::class)->findOneBy(['id' => $album_id]);
        if(!$album) return true;
        if($user->isFavourited($album))
            return true;
        $favourite = new Favourite();
        $favourite->setUser($user);
        $favourite->setAlbum($album);
        $this->entityManager->persist($favourite);
        $this->entityManager->flush();
        $user->addFavourite($favourite);
        $this->entityManager->flush();
        $album->addFavourite($favourite);
        $this->entityManager->flush();
        return false;
    }

    /**
     * @param $album_id
     * @param User $userEntity
     * @return bool
     */
    public function removeFromFavorites(int $album_id, User $user) : ?bool
    {
        $album = $this->entityManager->getRepository(Album::class)->findOneBy(['id' => $album_id]);
        if(!$album) return true;
        $favourite = $this->entityManager->getRepository(Favourite::class)->findOneBy(['user' => $user->getId(), 'album' => $album->getId()]);
        if(!$user->isFavourited($album))
            return false;
        $this->entityManager->remove($favourite);
        $this->entityManager->flush();
        return true;
    }

    /**
     * @param User $user
     * @return Album|null
     */
    public function setupAlbum(User $user) : ?Album
    {
        $album = new Album();
        $album->setUser($user);
        $album->setRating(0);
        $album->setGoldenlogo(0);
        return $album;
    }

    /**
     * @param Album $album
     * @return Song
     */
    public function setupSong(Album $album) : ?Song
    {
        $song = new Song();
        $song->setAlbum($album);
        return $song;
    }

    /**
     * @param Album $album
     * @param User $user
     * @return Comment
     */
    public function setupComment(Album $album, User $user) : ?Comment
    {
        $comment = new Comment();
        $comment->setAlbum($album);
        $comment->setUser($user);
        return $comment;
    }

    /**
     * @param Album $album
     * @param User $user
     * @return Report
     */
    public function setupReport(Album $album, User $user) : ?Report
    {
        $report = new Report();
        $report->setAlbum($album);
        $report->setUser($user);
        $report->setTime(time());
        return $report;
    }

    /**
     * @param Album $album
     * @param User $user
     * @return Donation
     */
    public function setupDonation(Album $album, User $user) : ?Donation
    {
        $donation = new Donation();
        $donation->setAlbum($album);
        $donation->setUser($user);
        return $donation;
    }

    /**
     * @param Song $song
     * @param User $user
     * @param Album $album
     */
    public function addSong(Song $song, User $user, Album $album)
    {
        if (filter_var($song->getSongUrl(), FILTER_VALIDATE_URL) === FALSE) {
            $this->session->getFlashBag()->add('song', 'Please add valid song URL.');
            return new RedirectResponse($this->router->generate('album_show', ['id' => $album->getId()]));
        }
        if(!strstr($song->getSongUrl(), 'https://soundcloud.com/'))
        {
            $this->session->getFlashBag()->add('song', 'Please add valid song URL.');
            return new RedirectResponse($this->router->generate('album_show', ['id' => $album->getId()]));
        }


        $this->entityManager->persist($song);
        $this->entityManager->flush();
        $this->activityService
            ->addActivity($user, 'Song', $album);
        $this->badgeService->setBadge($user, "First song");
        if($album->getSongs()->count() == 10) {
            $this->badgeService->setBadge($user, "10 songs");
        }
        $this->session->getFlashBag()->add('song', 'Song added.');
        return;
    }

    /**
     * @param Song $song
     * @param User $user
     * @param Album $album
     */
    public function addComment(Comment $comment, User $user, Album $album)
    {
        $this->entityManager->persist($comment);
        $this->entityManager->flush();
        $this->activityService
            ->addActivity($user, 'Comment', $album);
        $this->badgeService->setBadge($user, "First comment");
        if($user->getComments()->count() == 10) {
            $this->badgeService->setBadge($user, "10 comments!");
        }
        elseif($user->getComments()->count() == 100) {
            $this->badgeService->setBadge($user, "Knows a lot about music");
        }
        $this->session->getFlashBag()->add('comment', 'Comment added.');
        return;
    }

    /**
     * @param Report $report
     * @param User $user
     * @param Album $album
     */
    public function addReport(Report $report, User $user, Album $album)
    {
        $this->entityManager->persist($report);
        $this->entityManager->flush();
        $this->badgeService->setBadge($user, "First report");
        $this->session->getFlashBag()->add('report', 'You reported this album.');
        return;
    }

    /**
     * @param Donation $donation
     * @param User $user
     * @param Album $album
     * @param float|null $money
     * @param $token
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function makeDonation(Donation $donation, User $user, Album $album, float $money, $token)
    {
        $donation_money = $money * 100;
        if($donation_money < 50) {
            $this->session->getFlashBag()->add('donation', 'Lowest donation 50 cents');
            return new RedirectResponse($this->router->generate('album_show', ['id' => $album->getId()]));
        }
        if(!$this->donationService->payment($token, $album->getName(), $donation_money)) {
            $this->session->getFlashBag()->add('donation', 'Card is not usable.');
            return new RedirectResponse($this->router->generate('album_show', ['id' => $album->getId()]));
        }
        $this->donationService->changeBalance($album->getUser(), $donation_money);
        $this->session->getFlashBag()->add('donation', 'You donated for ' . $album->getName() . '.');
        $this->entityManager->persist($donation);
        $this->entityManager->flush();
        $this->badgeService->setBadge($user, "Donation");

    }

    /**
     * @param Album $album
     */
    public function createAlbum(Album $album)
    {
        $this->entityManager->persist($album);
        $this->entityManager->flush();

        return;
    }
}