<?php


namespace App\Services;

use App\Entity\User;
use App\Entity\Activity;
use App\Entity\Album;
use Doctrine\ORM\EntityManagerInterface;

class ActivityService
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function addActivity(User $user,string $action, Album $album)
    {
        if(!$user || !$album) return;

        $activity = new Activity();
        $setAction = "";
        switch($action)
        {
            case 'Add':
            {
                $setAction = "Created album '" . $album->getName() . "'.";
                break;
            }
            case 'Song':
            {
                $setAction = "Added song to '" . $album->getName() . "' album.";
                break;
            }
            case 'Comment':
            {
                $setAction = "Commented about '" . $album->getName() . "' album.";
                break;
            }
            case 'Edit':
            {
                $setAction = "Editted his album '" . $album->getName() . "'.";
                break;
            }
            case 'Like':
            {
                $setAction = "Likes '" . $album->getName() . "' album.";
                break;
            }
            case 'Favourite':
            {
                $setAction = "Added '" . $album->getName() . "' album to favourites.";
                break;
            }
            default: break;
        }

        $activity->setAction($setAction);
        $activity->setUser($user);
        $activity->setTime(time());
        $this->entityManager->persist($activity);
        $this->entityManager->flush();
        return;
    }
}