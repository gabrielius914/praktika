<?php

namespace App\DataFixtures\Tests;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixture extends Fixture
{
    public function load(ObjectManager $objectManager)
    {
        for ($i = 0; $i < 10; $i++) {
            $user = new User();
            $user->setUsername('username '.$i);
            $user->setPassword('$2y$13$7fopFNNlMRT0zRiZi3dbIO9hPPw5XR6UReaE4SLgXaVpXaGm8Eydi');
            $user->setRoles(['ROLE_ARTIST']);
            $objectManager->persist($user);
        }

        $objectManager->flush();
    }
}