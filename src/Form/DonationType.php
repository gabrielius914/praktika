<?php

namespace App\Form;

use App\Entity\Donation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;




class DonationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('money')
            ->add('card_num', null, array('label' => false, 'attr' => array('size' => 2), 'mapped' => false))
            ->add('cvc', null, array('label' => false, 'attr' => array('size' => 2), 'mapped' => false))
            ->add('exp_month', null, array('label' => false, 'attr' => array('size' => 2), 'mapped' => false))
            ->add('exp_year', null, array('label' => false, 'attr' => array(), 'mapped' => false))
            ->add('stripeToken', HiddenType::class, array('attr' => array('size' => 4), 'mapped' => false))

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Donation::class,
            'user' => null,
            'album' => null
        ]);
    }
}
