<?php

namespace App\Form;

use App\Entity\Song;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SongType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null,
                array(
                    'label' => false,
                    'attr' => array(
                        'placeholder' => 'Enter song name',
            )))
            ->add('song_url', null,
                array(
                    'label' => false,
                    'attr' => array(
                        'placeholder' => 'https://soundcloud.com/...',
            )))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Song::class,

        ]);
    }
}
