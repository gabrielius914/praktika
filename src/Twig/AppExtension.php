<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

Class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('price', [$this, 'formatPrice']),
        ];
    }

    public function formatPrice($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',') : ?string
    {
        $price = number_format(floatval($number / 100), $decimals, $decPoint, $thousandsSep);
        $price = $price. ' €';
        return $price;
    }
}