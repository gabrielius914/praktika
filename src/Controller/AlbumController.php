<?php

namespace App\Controller;

use App\Entity\Album;
use App\Form\AlbumType;
use App\Form\AlbumTypeEdit;
use App\Form\SongType;
use App\Form\CommentType;
use App\Form\ReportType;
use App\Form\DonationType;
use App\Entity\Song;
use App\Entity\Comment;
use App\Entity\Report;
use App\Entity\Donation;
use App\Repository\AlbumRepository;
use App\Repository\CommentRepository;
use App\Services\DonationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Services\AlbumService;
use App\Services\UserService;
use App\Services\ActivityService;
use App\Services\BadgeService;
use Symfony\Component\Security\Core\Security;


/**
 * @Route("/album")
 */
class AlbumController extends AbstractController
{
    private $security;
    private $activityService;
    private $badgeService;
    private $userService;
    private $albumService;
    private $donationService;

    /**
     * AlbumController constructor.
     * @param Security $security
     * @param ActivityService $activityService
     * @param BadgeService $badgeService
     * @param UserService $userService
     * @param AlbumService $albumService
     * @param DonationService $donationService
     */
    public function __construct(Security $security, ActivityService $activityService, BadgeService $badgeService, UserService $userService, AlbumService $albumService, DonationService $donationService)
    {
        $this->security = $security;
        $this->activityService = $activityService;
        $this->badgeService = $badgeService;
        $this->userService = $userService;
        $this->albumService = $albumService;
        $this->donationService = $donationService;
    }

    /**
     * @Route("", name="album_index", methods={"GET"})
     * @param AlbumRepository $albumRepository
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(AlbumRepository $albumRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $allAlbumsQuery = $albumRepository->createQueryBuilder('p')
            ->orderBy('p.rating', 'DESC')
            ->getQuery();
        $albums = $paginator->paginate(
            $allAlbumsQuery,
            $request->query->getInt('page', 1),
            // Items per page
            12
        );
        return $this->render('album/index.html.twig', [
            'albums' => $albums,

        ]);
    }

    /**
     * @Route("/new", name="album_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        if(!$this->security->isGranted('IS_AUTHENTICATED_FULLY'))
            throw $this->createNotFoundException('Only authenticated users can visit this page.');
        if($this->getuser()->getAccountType() == 0)
            throw $this->createNotFoundException('Only artists can visit this page.');


        $album = $this->albumService->setupAlbum($this->getUser());
        $form = $this->createForm(AlbumType::class, $album);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->albumService->createAlbum($album);
            return $this->redirectToRoute('profile');
        }
        return $this->render('album/new.html.twig', [
            'album' => $album,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="album_show", methods={"GET", "POST"})
     * @param Album $album
     * @param Request $request
     * @param CommentRepository $commentRepository
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function show(Album $album, Request $request, CommentRepository $commentRepository, PaginatorInterface $paginator): Response
    {
        if($this->userService->isAdmin() == false && $this->isGranted('IS_AUTHENTICATED_FULLY'))
        {
            $song = $this->albumService->setupSong($album);
            $comment = $this->albumService->setupComment($album, $this->getUser());
            $report = $this->albumService->setupReport($album, $this->getUser());
            $donation = $this->albumService->setupDonation($album, $this->getUser());
            $formSong = $this->createForm(SongType::class, $song);
            $formComment = $this->createForm(CommentType::class, $comment);
            $formReport = $this->createForm(ReportType::class, $report);
            $formDonation = $this->createForm(DonationType::class, $donation, array('attr' => array('id' => 'paymentFrm')));
            $formSong->handleRequest($request);
            $formComment->handleRequest($request);
            $formReport->handleRequest($request);
            $formDonation->handleRequest($request);

            if ($formSong->isSubmitted() && $formSong->isValid()) {
                $this->albumService->addSong($song, $this->getUser(), $album);
                return $this->redirectToRoute('album_show', ['id' => $album->getId()]);
            }
            else if($formComment->isSubmitted() && $formComment->isValid()) {
                $this->albumService->addComment($comment, $this->getUser(), $album);
                return $this->redirectToRoute('album_show', ['id' => $album->getId()]);
            }
            else if($formReport->isSubmitted() && $formReport->isValid()) {
                $this->albumService->addReport($report, $this->getUser(), $album);
                return $this->redirectToRoute('album_show', ['id' => $album->getId()]);
            }
            else if($formDonation->isSubmitted() && $formDonation->isValid()) {
                $this->albumService->makeDonation($donation, $this->getUser(), $album, $formDonation->get('money')->getData(), $request->request->get("stripeToken"));
                return $this->redirectToRoute('album_show', ['id' => $album->getId()]);
            }

            $allComments = $commentRepository->createQueryBuilder('p')->andWhere('p.album = :albumid')
                ->setParameter('albumid', $album->getId())
                ->getQuery();

            $comments = $paginator->paginate(
                $allComments,
                $request->query->getInt('comments', 1),
                // Items per page
                10,
                ['pageParameterName' => 'comments']
            );

            return $this->render('album/show.html.twig', [
                'album' => $album,
                'comments' => $comments,
                'formSong' => $formSong->createView(),
                'formComment' => $formComment->createView(),
                'formReport' => $formReport->createView(),
                'formDonation' => $formDonation->createView(),
            ]);
        }

        $allComments = $commentRepository->createQueryBuilder('p')->andWhere('p.album = :albumid')
            ->setParameter('albumid', $album->getId())
            ->getQuery();
        $comments = $paginator->paginate(
            $allComments,
            $request->query->getInt('comments', 1),
            // Items per page
            10,
            ['pageParameterName' => 'comments']
        );
        return $this->render('album/show.html.twig', [
            'album' => $album,
            'comments' => $comments,

        ]);
    }

    /**
     * @Route("/{id}/edit", name="album_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Album $album
     * @return Response
     */

    public function edit(Request $request, Album $album): Response
    {
        if(!$this->security->isGranted('IS_AUTHENTICATED_FULLY'))
            throw $this->createNotFoundException('You are not allowed to visit this page.');
        if (!$this->userService->isArtist() && !$this->userService->isOwner($album, $this->getUser())) {
            throw $this->createNotFoundException('You are not allowed to visit this page.');
        }
        if ($this->userService->isAdmin() == false) {
            $form = $this->createForm(AlbumTypeEdit::class, $album);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $this->getDoctrine()->getManager()->flush();
                $this->activityService
                    ->addActivity($this->getUser(), 'Edit', $album);
                $this->badgeService->setBadge($this->getUser(), "Editor mode");
                return $this->redirectToRoute('album_index', [
                    'id' => $album->getId(),
                ]);
            }
            return $this->render('album/edit.html.twig', [
                'album' => $album,
                'form' => $form->createView(),
            ]);
        }
        else {
            return $this->render('album/edit.html.twig', [
                'album' => $album,
            ]);
        }
    }

    /**
     * @Route("/{id}", name="album_delete", methods={"DELETE"})
     * @param Request $request
     * @param Album $album
     * @param AlbumService $albumService
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */

    public function delete(Request $request, Album $album) : Response
    {
        if(!$this->security->isGranted('IS_AUTHENTICATED_FULLY'))
            throw $this->createNotFoundException('You are not allowed to visit this page.');

        if(!$this->userService->isArtist() && !$this->userService->isOwner($album, $this->getUser()) && !$this->userService->isAdmin()) {
            return $this->redirectToRoute('album_index');
        }
        if ($this->isCsrfTokenValid('delete'.$album->getId(), $request->request->get('_token'))) {
            $this->albumService->deleteAllAlbumData($album);
        }
        return $this->redirectToRoute('album_index');
    }

    /**
     * @Route("/givelike/{id}", name="album_like", methods={"POST"})
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function giveLike($id, Request $request) : Response
    {
        if(!$this->security->isGranted('IS_AUTHENTICATED_FULLY'))
            throw $this->createNotFoundException('You are not allowed to do this action.');
        if ($request->isXMLHttpRequest()) {
            $isLiked = $this->albumService->setLikeForUserAndAlbum($id, $this->getUser());
            if($isLiked) {
                return $this->json(['error' => 'Already liked']);
            }
            else {
            $entityManager = $this->getDoctrine()->getManager();
            $album = $entityManager->getRepository(Album::class)->find($id);
                $this->activityService
                    ->addActivity($this->getUser(), 'Like', $album);
                $this->badgeService->setBadge($this->getUser(), "First like");
            }
            return $this->json(['data' => $id]);
        }
        return new Response('This is not ajax!', 400);
    }

    /**
     * @Route("/remove-like/{id}", name="album_unlike", methods={"POST"})
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function removeLike($id, Request $request) : Response
    {
        if(!$this->security->isGranted('IS_AUTHENTICATED_FULLY'))
            throw $this->createNotFoundException('You are not allowed to do this action.');
        if ($request->isXMLHttpRequest()) {
            $isLiked = $this->albumService->removeLikeForUserAndAlbum($id, $this->getUser());
            if(!$isLiked) {
                return Response('Not liked yet.', 400);
            }
            return new JsonResponse(array('data' => $id));
        }
        return new Response('This is not ajax!', 400);
    }

    /**
     * @Route("/addtofavourites/{id}", name="album_favourite", methods={"POST"})
     * @param $id
     * @param Request $request
     * @return JsonResponse|Response
     */

    public function addToFavourites($id, Request $request) : Response
    {
        if(!$this->security->isGranted('IS_AUTHENTICATED_FULLY'))
            throw $this->createNotFoundException('You are not allowed to do this action.');
        if ($request->isXMLHttpRequest()) {
            $isFavourited = $this->albumService->addToFavorites($id, $this->getUser());
            if($isFavourited) {
                return Response('Already in your favourite list!', 400);
            }
            else {
                $entityManager = $this->getDoctrine()->getManager();
                $album = $entityManager->getRepository(Album::class)->findOneBy(['id' => $id]);
                $this->activityService
                    ->addActivity($this->getUser(), 'Favourite', $album);
                $this->badgeService->setBadge($this->getUser(), "First favorite album");
            }
            return $this->json(['data' => $id]);
        }
        return new Response('This is not ajax!', 400);
    }

    /**
     * @Route("/remove-from-favourites/{id}", name="album_remove_favourite", methods={"POST"})
     * @param $id
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function removeFromFavourites($id, Request $request) : Response
    {
        if(!$this->security->isGranted('IS_AUTHENTICATED_FULLY'))
            throw $this->createNotFoundException('You are not allowed to do this action.');
        if ($request->isXMLHttpRequest()) {
            $isFavourited = $this->albumService->removeFromFavorites($id, $this->getUser());
            if(!$isFavourited)
                return Response('Not in your favourite list!', 400);
            return new JsonResponse(array('data' => $id));
        }
        return new Response('This is not ajax!', 400);
    }

}
