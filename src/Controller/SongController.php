<?php

namespace App\Controller;

use App\Entity\Song;
use App\Entity\Album;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/song")
 */
class SongController extends AbstractController
{
    /**
     * @Route("/{id}/{album}", name="song_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Song $song, Album $album): Response
    {
        if ($this->isCsrfTokenValid('delete'.$song->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($song);
            $entityManager->flush();
        }
        $this->addFlash('song', 'Song was deleted.');
        return $this->redirectToRoute('album_show', ['id' => $album->getId()]);
    }
}
