<?php

namespace App\Controller;

use App\Services\BadgeService;
use App\Services\DonationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use App\Entity\Album;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;


/**
 * Class StoreController
 * @package App\Controller
 * @Route("/store")
 */
class StoreController extends AbstractController
{
    /**
     * @Route("/", name="store_index", methods={"GET", "POST"})
     * @param Request $request
     * @param TokenStorageInterface $tokenStorage
     * @param BadgeService $badgeService
     * @param DonationService $donationService
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function index(BadgeService $badgeService, DonationService $donationService, Request $request) : Response
    {
        if(!$this->isGranted('IS_AUTHENTICATED_FULLY') || !$this->getUser()->getAccountType() == 1)
            throw $this->createNotFoundException('Only authenticated users can visit this page.');
        $albums = $donationService->getUserAlbums($this->getUser());
        $defaultData = ['message' => 'Rise your album!'];
        $formRating = $this->createFormBuilder($defaultData)
            ->add('album', ChoiceType::class, [
                'label' => false,
                'choices' => [
                    $albums
                ],
                'choice_label' => function(Album $album, $key, $value) {
                    return strtoupper($album->getName());
                },
                'group_by' => function() {
                    return 'Your albums';
                },])->getForm();

        $diffdata = ['message' => 'Golden logo'];
        $formGold = $this->createFormBuilder($diffdata)
            ->add('album2', ChoiceType::class, [
                'label' => false,
                'choices' => [
                    $albums
                ],
                'choice_label' => function(Album $album, $key, $value) {
                    return strtoupper($album->getName());
                },
                'group_by' => function() {
                    return 'Your albums';
                },])->getForm();


        $formRating->handleRequest($request);
        $formGold->handleRequest($request);

        if ($formRating->isSubmitted() && $formRating->isValid()) {
            $donationService->increaseRating($formRating->get('album')->getData(), $this->getUser());
            return $this->redirectToRoute('store_index');
        }
        else if($formGold->isSubmitted() && $formGold->isValid()) {
            $donationService->goldenLogo($formGold->get('album2')->getData(), $this->getUser());
            return $this->redirectToRoute('store_index');
        }
        return $this->render('store/index.html.twig', [
            'formRating' => $formRating->createView(),
            'formGold' => $formGold->createView()
        ]);
    }
}
