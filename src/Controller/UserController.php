<?php

namespace App\Controller;

use App\Entity\Activity;

use App\Entity\User;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Services\UserService;
use Symfony\Component\Security\Core\Security;
use App\Repository\FavouriteRepository;


/**
 * Class UserController
 * @package App\Controller
 */
class UserController extends AbstractController
{
    /**
     * @Route("/profile", name="profile", methods={"GET"})
     */
    public function profile(Security $security) : Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if($this->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('album_index');
        }
        $albums = $security->getUser()->getAlbums();
        $activities = $this->getDoctrine()->getManager()->getRepository(Activity::class)->getUserActivities($security->getUser()->getId());
        return $this->render('user/profile.html.twig', [
            'albums' => $albums,
            'activities' => $activities,
        ]);
    }

    /**
     * @Route("/profile/{id}", name="public_profile", methods={"GET"})
     */
    public function userProfile(User $user, Request $request) : Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if($this->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('album_index');
        }

        if(!$this->getDoctrine()->getManager()->getRepository(User::class)->findOneBy(['id' => $user->getId()]) )
            throw $this->createNotFoundException('This profile doesnt exist');

        if($user->getId() == $this->getUser()->getId())
            return $this->redirectToRoute('profile');

        $albums = $user->getAlbums();
        $activities = $this->getDoctrine()->getManager()->getRepository(Activity::class)->getUserActivities($user->getId());
        return $this->render('user/public_profile.html.twig', [
            'user' => $user,
            'albums' => $albums,
            'activities' => $activities,
        ]);
    }

    /**
     * @Route("/favourites", name="user_favourites")
     * @param Security $security
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function favourites(Security $security, FavouriteRepository $favouriteRepository, PaginatorInterface $paginator, Request $request) : Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if($this->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('album_index');
        }
        $allfavouriteQuery = $favouriteRepository->createQueryBuilder('p')
            ->andWhere('p.user = :userid')
            ->setParameter('userid', $this->getUser()->getId())
            ->getQuery();
        $favourites = $paginator->paginate(
            $allfavouriteQuery,
            $request->query->getInt('fav', 1),
            // Items per page
            20,
            ['pageParameterName' => 'fav']
        );
        return $this->render('user/favourites.html.twig', [
           'favourites' => $favourites,
        ]);
    }

    /**
     * @Route("/albums", name="user_albums", methods={"GET"})
     * @return Response
     */
    public function albums(): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if($this->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('album_index');
        }
        return $this->render('user/albums.html.twig');
    }
}
