<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\ReportRepository;
use App\Repository\UserRepository;
use App\Services\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Album;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;
use App\Repository\AlbumRepository;
use App\Entity\Report;


/**
 * Class AdminController
 * @package App\Controller
 * @Route("/admin")
 * @Security("has_role('ROLE_ADMIN')")
 */
class AdminController extends AbstractController
{

    /**
     * @Route("", name="admin_index")
     * @param AlbumRepository $albumRepository
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(AlbumRepository $albumRepository, Request $request, PaginatorInterface $paginator) : Response
    {
        $allAlbumsQuery = $albumRepository->createQueryBuilder('p')
            ->getQuery();
        $albums = $paginator->paginate(
            $allAlbumsQuery,
            $request->query->getInt('albums', 1),
            // Items per page
            20,
            ['pageParameterName' => 'albums']);
        return $this->render('admin/index.html.twig', [
            'albums' => $albums,
        ]);
    }

    /**
     * @Route("/reports", name="admin_reports", methods={"GET"})
     * @param ReportRepository $reportRepository
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function reports(ReportRepository $reportRepository, Request $request, PaginatorInterface $paginator) : Response
    {
        $allReportsQuery = $reportRepository->createQueryBuilder('p')
            ->getQuery();
        $reports = $paginator->paginate(
          $allReportsQuery,
          $request->query->getInt('reports', 1),
          20,
          ['pageParameterName' => 'reports']);
        return $this->render('admin/reports.html.twig', [
           'reports' => $reports,
        ]);
    }

    /**
     * @Route("/users", name="admin_users", methods={"GET"})
     * @param UserRepository $userRepository
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function users(UserRepository $userRepository, Request $request, PaginatorInterface $paginator) : Response
    {
        $allUsersQuery = $userRepository->createQueryBuilder('p')
            ->getQuery();
        $users = $paginator->paginate(
            $allUsersQuery,
            $request->query->getInt('users', 1),
            20,
            ['pageParameterName' => 'users']);
        return $this->render('admin/users.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @Route("/delete-user/{id}", name="admin_user_delete", methods={"DELETE"})
     * @param Request $request
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteUsers(Request $request, User $user, UserService $userService)
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $userService->deleteUserData($user);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }
        $this->addFlash('admin', 'User account was deleted.');
        return $this->redirectToRoute('admin_users');
    }

    /**
     * @Route("/{id}", name="admin_report_delete", methods={"DELETE"})
     * @param Request $request
     * @param Report $report
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Request $request, Report $report) : Response
    {
        if ($this->isCsrfTokenValid('delete'.$report->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($report);
            $entityManager->flush();
        }
        $this->addFlash('admin', 'Report was deleted.');
        return $this->redirectToRoute('admin_reports');
    }
}
