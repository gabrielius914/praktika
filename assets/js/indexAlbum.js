
    window.Like = function Like(id) {
        var str = "[id='like" + id + "']";
        var status = parseInt($(str)[0].dataset.status);

        if (status === 1) /* Liked */
        {
            $.ajax({
                type: "POST",
                url: getUnlikeUrl(id),
                data: {
                    id: id
                },
                success: function (data) {

                    var str = "[id='like" + data['data'] + "']";
                    $(str)[0].dataset.status = 0;
                    $(str)[0].dataset.likes = parseInt($(str)[0].dataset.likes) - 1;
                    $(str)[0].innerHTML = "<i class=\"fas fa-thumbs-up\"></i> " + (parseInt($(str)[0].dataset.likes)) + " Liked";

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });
        } else /* Not liked */
        {
            $.ajax({
                type: "POST",
                url: getLikeUrl(id),
                data: {
                    id: id
                },
                success: function (data) {

                    var str = "[id='like" + data['data'] + "']";
                    $(str)[0].dataset.status = 1;
                    $(str)[0].dataset.likes = parseInt($(str)[0].dataset.likes) + 1;
                    $(str)[0].innerHTML = "<i class=\"fas fa-thumbs-up\"></i> " + $(str)[0].dataset.likes + " Click to unlike";

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });
        }
    }

    window.Favorite = function Favorite(id) {
        var str = "[id='favourite" + id + "']";
        var status = parseInt($(str)[0].dataset.status);


        if (status === 1) /* Favorited */
        {
            $.ajax({
                type: "POST",
                url: getFavoriteRemoveUrl(id),
                data: {
                    id: id
                },
                success: function (data) {

                    var str = "[id='favourite" + data['data'] + "']";
                    $(str)[0].innerHTML = "<i class=\"fas fa-heart\"></i> Add to Favourites";
                    $(str)[0].dataset.status = 0;
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });
        } else /* Not favorited */
        {
            $.ajax({
                type: "POST",
                url: getFavoriteUrl(id),
                data: {
                    id: id
                },
                success: function (data) {

                    var str = "[id='favourite" + data['data'] + "']";
                    $(str)[0].dataset.status = 1;
                    $(str)[0].innerHTML = "<i class=\"fas fa-heart\"></i> " + "Remove from Favourites";

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });
        }
    }

    window.getLikeUrl = function getLikeUrl(id) {

        var str = "[id='like" + id + "']";
        console.log($(str)[0].dataset);
        return $(str)[0].dataset.like;
    }

    window.getUnlikeUrl = function getUnlikeUrl(id) {

        var str = "[id='like" + id + "']";
        console.log($(str)[0].dataset);
        return $(str)[0].dataset.unlike;
    }

    window.getFavoriteUrl = function getFavoriteUrl(id) {
        var str = "[id='favourite" + id + "']";
        return $(str)[0].dataset.add;
    }

    window.getFavoriteRemoveUrl = function getFavoriteRemoveUrl(id) {
        var str = "[id='favourite" + id + "']";
        return $(str)[0].dataset.remove;
    }

