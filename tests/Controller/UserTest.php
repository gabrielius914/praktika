<?php

namespace AppBundle\Tests\Controller;

use App\Entity\User;

use Doctrine\ORM\Tools\SchemaTool;
use \Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class UserTest extends WebTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    protected static $application;


    public function setUp()
    {
        $kernel = static::createKernel();
        $kernel->boot();

        $this->em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $schemaTool = new SchemaTool($this->em);
        $metadata = $this->em->getMetadataFactory()->getAllMetadata();

        $schemaTool->dropSchema($metadata);
        $schemaTool->createSchema($metadata);

        /**
         * @var Client $client
         */
        $client = static::createClient();
        $this->em = $client->getContainer()->get('doctrine.orm.entity_manager');


    }

    public function testAddUser()
    {

        $user = new User();
        $user->setUsername('testinisVartotojas');
        $user->setPassword('$2y$13$7fopFNNlMRT0zRiZi3dbIO9hPPw5XR6UReaE4SLgXaVpXaGm8Eydi');
        $user->setAccountType(1);
        $user->setRoles(['ROLE_ARTIST']);

        $this->em->persist($user);
        $this->em->flush();


        $user = $this->em->getRepository(User::class)->findOneBy(['username' => 'testinisVartotojas']);

        $this->assertEquals('testinisVartotojas', $user->getUsername());

        $this->em->remove($user);
        $this->em->flush();

        $user = new User();
        $user->setUsername('testinisVartotojas');
        $user->setPassword('$2y$13$7fopFNNlMRT0zRiZi3dbIO9hPPw5XR6UReaE4SLgXaVpXaGm8Eydi');
        $user->setAccountType(1);
        $user->setRoles(['ROLE_ARTIST']);

        $this->em->persist($user);
        $this->em->flush();


        $user = $this->em->getRepository(User::class)->findOneBy(['username' => 'testinisVartotojas2']);

        $this->assertEquals('testinisVartotojas', $user->getUsername());

        $this->em->remove($user);
        $this->em->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function tearDown()
    {
        parent::tearDown();
        $this->em->close();
    }
}