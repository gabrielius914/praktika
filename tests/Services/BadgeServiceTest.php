<?php

namespace App\Tests\Services;

use App\Services\BadgeService;

use PHPUnit\Framework\TestCase;

class BadgeServiceTest extends TestCase
{
    public $badgeService;
    public function __construct($name = null, array $data = [], $dataName = '', BadgeService $badgeService)
    {
        parent::__construct($name, $data, $dataName);
    }

    public function getIsValidBadgeData()
    {
        $out = [];

        $out['test1'] = [false, 'Badge'];
        $out['test2'] = [true, 'First comment'];
        return $out;
    }


    /**
     * @dataProvider getIsValidBadgeData
     */
    public function testIsValidBadge(bool $expected, string $badgeName)
    {

        $result = $badgeService->isValidBadge($badgeName);

        $this->assertEquals($result, $expected);
    }
}